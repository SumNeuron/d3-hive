import * as d3 from 'd3'

export default function hive(container) {
  let
  namespace='hive',
  n = (s) => `${namespace}-${s}`,

  data,
  dataKeys,
  dataKeysOrder,

  values, // flattened version of dataValues
  dataValues,
  valueExtractor = (key, sub, pay) => pay,
  valuesExtent,

  radii, // flattened version of dataRadii
  dataRadii,
  radiusExtractor = (key, sub, pay)=>5,
  scaleR = d3.scaleLinear().range([3,10]),
  radiiExtent,

  spaceX,
  spaceY,
  spaceRelax = 20,

  forceCollide = 4,
  simulation,
  simulationTicks=120,

  scaleX = d3.scaleLinear(),
  axisX,
  axisXTicks,
  gridlinesX,
  gAxisX,

  scaleY = d3.scaleBand(),
  axisY,
  axisYTicks,
  gridlinesY=true,
  gAxisY,

  includeCountScale=true,
  scaleC = d3.scaleBand(),
  axisC,
  gAxisC,


  nodes,

  colorExtractor = (key, sub, pay) => {
    return d3.scaleSequential()
    .interpolator(d3.interpolateViridis)
    (dataKeys.indexOf(key) / dataKeys.length)
  },
  stroke = (key, sub, pay)=>colorExtractor(key, sub, pay),
  strokeWidth = 0.1,
  opacity = 0.5,

  transitionDuration = (key, sub, pay)=>500,
  axesTransitionDuration = 500,
  easeFn = d3.easeSin,
  mouseover = (k, i) => {},
  mouseleave = (k, i) => {}

  hive.namespace = function(_) { return arguments.length ? (namespace = _, hive) : namespace; };

  hive.data = function(_) { return arguments.length ? (data = _, hive) : data; };
  hive.dataKeys = function(_) { return arguments.length ? (dataKeys = _, hive) : dataKeys; };
  hive.dataKeysOrder = function(_) { return arguments.length ? (dataKeysOrder = _, hive) : dataKeysOrder; };

  hive.values = function(_) { return arguments.length ? (values = _, hive) : values; };
  hive.dataValues = function(_) { return arguments.length ? (dataValues = _, hive) : dataValues; };
  hive.valueExtractor = function(_) { return arguments.length ? (valueExtractor = _, hive) : valueExtractor; };
  hive.valuesExtent = function(_) { return arguments.length ? (valuesExtent = _, hive) : valuesExtent; };

  hive.dataRadii = function(_) { return arguments.length ? (dataRadii = _, hive) : dataRadii; };
  hive.radiusExtractor = function(_) { return arguments.length ? (radiusExtractor = _, hive) : radiusExtractor; };
  hive.scaleR = function(_) { return arguments.length ? (scaleR = _, hive) : scaleR; };

  hive.spaceX = function(_) { return arguments.length ? (spaceX = _, hive) : spaceX; };
  hive.spaceY = function(_) { return arguments.length ? (spaceY = _, hive) : spaceY; };
  hive.spaceRelax = function(_) { return arguments.length ? (spaceRelax = _, hive) : spaceRelax; };

  hive.forceCollide = function(_) { return arguments.length ? (forceCollide = _, hive) : forceCollide; };
  hive.simulation = function(_) { return arguments.length ? (simulation = _, hive) : simulation; };
  hive.simulationTicks = function(_) { return arguments.length ? (simulationTicks = _, hive) : simulationTicks; };

  hive.scaleX = function(_) { return arguments.length ? (scaleX = _, hive) : scaleX; };
  hive.axisX = function(_) { return arguments.length ? (axisX = _, hive) : axisX; };
  hive.axisXTicks = function(_) { return arguments.length ? (axisXTicks = _, hive) : axisXTicks; };
  hive.gridlinesX = function(_) { return arguments.length ? (gridlinesX = _, hive) : gridlinesX; };
  hive.gAxisX = function(_) { return arguments.length ? (gAxisX = _, hive) : gAxisX; };


  hive.scaleY = function(_) { return arguments.length ? (scaleY = _, hive) : scaleXY; };
  hive.axisY = function(_) { return arguments.length ? (axisY = _, hive) : axisXY; };
  hive.gridlinesY = function(_) { return arguments.length ? (gridlinesY = _, hive) : gridlinesY; };
  hive.gAxisY = function(_) { return arguments.length ? (gAxisY = _, hive) : gAxisY; };

  hive.includeCountScale = function(_) { return arguments.length ? (includeCountScale = _, hive) : includeCountScale; };
  hive.scaleC = function(_) { return arguments.length ? (scaleC = _, hive) : scaleC; };
  hive.axisC = function(_) { return arguments.length ? (axisC = _, hive) : axisC; };
  hive.gAxisC = function(_) { return arguments.length ? (gAxisC = _, hive) : gAxisC; };








  hive.nodes = function(_) { return arguments.length ? (nodes = _, hive) : nodes; };

  hive.colorExtractor = function(_) { return arguments.length ? (colorExtractor = _, hive) : colorExtractor; };
  hive.stroke = function(_) { return arguments.length ? (stroke = _, hive) : stroke; };
  hive.strokeWidth = function(_) { return arguments.length ? (strokeWidth = _, hive) : strokeWidth; };
  hive.opacity = function(_) { return arguments.length ? (opacity = _, hive) : opacity; };

  hive.transitionDuration = function(_) { return arguments.length ? (transitionDuration = _, hive) : transitionDuration; };
  hive.axesTransitionDuration = function(_) { return arguments.length ? (axesTransitionDuration = _, hive) : axesTransitionDuration; };
  hive.easeFn = function(_) { return arguments.length ? (easeFn = _, hive) : easeFn; };


  hive.mouseover = function(_) { return arguments.length ? (mouseover = _, hive) : mouseover; };
  hive.mouseleave = function(_) { return arguments.length ? (mouseleave = _, hive) : mouseleave; };


  function hive() {
    dataKeys = d3.keys(data)

    values = []
    radii = []
    dataRadii = []
    dataValues = dataKeys.map((k,i)=>{
      let subKeys = d3.keys(data[k])
      let subRad = []
      let vals = subKeys.map((s, j)=>{
        let val = valueExtractor(k, s, data[k][s])
        let rad = radiusExtractor(k, s, data[k][s])
        values.push({key:k, sub:s, val: val})
        radii.push(rad)
        subRad.push(rad)
        return val
      })
      dataRadii.push(subRad)
      return vals
    })

    radiiExtent = d3.extent(radii)
    scaleR.domain(radiiExtent)
    valuesExtent = d3.extent(values.map(({val})=>val))
    let [dataMin, dataMax] = valuesExtent

    scaleX.range([0+spaceRelax, spaceX-2*spaceRelax]).domain([dataMin, dataMax])
    axisX = d3.axisBottom(scaleX).ticks(axisXTicks)
    if (gridlinesX) axisX.tickSize(-spaceY)
    gAxisX = container.select('g.x-axis')
    if (gAxisX.empty()) gAxisX = container.append('g').attr('class', 'x-axis')
    gAxisX
    .transition(axesTransitionDuration)
    .ease(easeFn)
    .attr('transform',`translate(${0}, ${spaceY})`).call(axisX)



    // primary y axis
    let subSpaceY = spaceY / (dataKeys.length)
    // adjustedSpaceY = spaceY - categorySpaceY

    scaleY = d3.scaleBand().domain(dataKeys).paddingInner(1)
    if (dataKeysOrder !== undefined) {
      scaleY.domain(dataKeysOrder.filter(e=>dataKeys.indexOf(e)>-1))
    }

    scaleY.range([spaceY-subSpaceY/2, subSpaceY/2])//*spaceY-adjustedSpaceY

    axisY = d3.axisLeft(scaleY)
    if (gridlinesY) axisY.tickSize(-spaceX+1*spaceRelax)
    gAxisY = container.select('g.y-axis')
    if (gAxisY.empty()) gAxisY = container.append('g').attr('class', 'y-axis')
    gAxisY
    .transition(axesTransitionDuration)
    .ease(easeFn)
    .call(axisY)

    if (includeCountScale) {
      scaleC = d3.scaleBand()
        .domain(dataKeys)
        .paddingInner(1)
        .range([spaceY-subSpaceY/2, subSpaceY/2])

      axisC = d3.axisRight(scaleC)
      .tickFormat(function(d, i){
        return dataValues[i].length
      })

      gAxisC = container.select('g.c-axis')
      if (gAxisC.empty()) gAxisC = container.append('g').attr('class', 'c-axis')

      gAxisC
      .transition(axesTransitionDuration)
      .ease(easeFn)
      .attr('transform',`translate(${spaceX-spaceRelax}, ${0})`)
      .call(axisC)

      gAxisC.selectAll(".tick text")
        .attr("dx", scaleR.range()[1]);
    } else {
      gAxisC.selectAll('*').remove()
    }






     simulation = d3.forceSimulation(values)
      .force('x', d3.forceX(function(d, i) {
        return scaleX(d.val)
      }).strength(1))
      .force('y', d3.forceY(function(d, i){
        return scaleY(d.key)
      }).strength(1))
      .force('collide', d3.forceCollide(forceCollide))
      .stop();

    for (var i = 0; i < simulationTicks; ++i) simulation.tick();

    let gNodes = container.select(`g.${n('nodes-container')}`)
    if (gNodes.empty()) gNodes = container.append('g').attr('class',`${n('nodes-container')}`)

    let voroni = d3.voronoi()
      .extent([[0-spaceRelax,0-spaceRelax], [spaceX+spaceRelax, spaceY+spaceRelax]])
      .x((d)=>d.x)
      .y((d)=>d.y)
      .polygons(values)

    nodes = gNodes.selectAll(`circle.${n('nodes')}`).data(voroni)
    nodes.exit().remove()
    nodes = nodes.merge(
      nodes.enter()
      .append('circle')
      .attr('class', function(d,i){
        let  s = `${n('nodes')}`
        if (d !== undefined) s += ` ${n('group-'+d.data.key)}`
        return s
      })
    )
    nodes
    .transition(function(d, i) {
      let k = d.data.key, s = d.data.sub
      return transitionDuration(k, s, data[k][s])
    })
    .ease(easeFn)
    .attr('r', function(d, i) {
      if (d === undefined) return 0
      let k = d.data.key, s = d.data.sub
      let r = radiusExtractor(k, s, data[k][s])
      return scaleR(r)
    })
    .attr('cx', function(d, i){
      if (d === undefined) return 0
      return d.data.x
    })
    .attr('cy', function(d, i){
      if (d === undefined) return 0
      return d.data.y
    })
    .attr('fill', function(d, i) {
      if (d === undefined) return 'transparent'
      let k = d.data.key, s = d.data.sub
      return colorExtractor(k, s, data[k][s])
    })
    .attr('stroke', function(d, i) {
      if (d === undefined) return 'transparent'
      let k = d.data.key, s = d.data.sub
      return stroke(k, s, data[k][s])
    })
    .attr("stroke-width", strokeWidth)
    .attr('opacity', opacity)

    nodes
    .on('mouseenter', mouseover)
    .on('mouseover',  mouseover)
    .on('mouseleave', mouseleave)
    .on('mouseexit',  mouseleave)

  }


  return hive
}
