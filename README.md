# d3-hive
A simple hive-swarm plot


# Install

```
npm i d3-hive
```

# Usage

```js
import {hive} from 'd3-hive'
```

## Assumptions

It is assumed that the `data` bound to `hive` is an object, where key corresponds
to a "hive" in the swarm

```js
data = {
  'A': 10,
  'B': 20,
  ...
}
```

more complex data can be provided should utility functions such as `valueExtractor`
be overwritten from their default behavior:

```js
data = {
  A: {
    x: { value: 1 },
    y: { value: 2 },
    ...
  },
  ...
}
```
