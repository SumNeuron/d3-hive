import nodeResolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import babel from 'rollup-plugin-babel';
import uglify from 'rollup-plugin-uglify-es';
import minimist from 'minimist';


import pkg from './package.json';

const argv = minimist(process.argv.slice(2));

const config = {
  input: 'src/entry.js',
  external: ['d3'],
  output: {
    name: pkg.name,
    exports: 'named',
    extend: true,
    // external: ['d3'],
    globals: {
      d3: 'd3'
    },
  },
  plugins: [
    babel({
      exclude: 'node_modules/**',
      externalHelpers: true,
    //   runtimeHelpers: true,
    }),
    nodeResolve({
      mainFields: [
        'module',
        'main', 'jsnext'
      ]
    }),
    commonjs({
      include: 'node_modules/**',
      extensions: [ '.js', '.coffee' ],
      ignore: [ 'conditional-runtime-dependency' ]
    })

  ],
  onwarn: function ( message ) {
    if (message.code === 'CIRCULAR_DEPENDENCY') {
      return;
    }
    console.error(message);
  }
};

// Only minify browser (iife) version
if (argv.format === 'iife') {
  config.plugins.push(uglify());
}

export default config;
